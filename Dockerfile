FROM python:3.11-bookworm

RUN pip install "python-socketio[client]"
RUN pip install "python-dotenv"
RUN pip install "discord.py"
RUN pip install "aiohttp"

WORKDIR /app

COPY . .

CMD ["python", "datbot.py"]
