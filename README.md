# datbot

`datbot` is a Discord bot which alerts a text channel when a DatDrop battle is started.

## Usage

`$ DISCORD_TOKEN='<api token>' DISCORD_CHANNEL='<channel id>' python datbot.py`

## Commands

The bot listens for the following commands in the given channel:

|       **Command**       | **Example** |          **Description**          |
|:-----------------------:|:-----------:|:---------------------------------:|
| !limit <$dollar amount> |  !limit 250 | Sets the alert threshold to $250. |
| !profit <$steam id>     |  !profit 76561198309053338 | Returns the total profit of the user. |
