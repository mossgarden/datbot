'''
datbot.py
'''

import aiohttp
import asyncio
import discord
import dotenv
import socketio
import os

def datdrop_url():
    return 'https://socket.datdrop.com?client_id=1&x_client_id=17024182081300.6151851679327791&EIO=3'

def datdrop_user_url(steam_id):
    return f'https://api.datdrop.com/api/profile/{steam_id}?with_statistic=1'

def datdrop_history_url(steam_id, offset = 0):
    return f'https://api.datdrop.com/api/group/list/games/history/{steam_id}?offset={offset}'

def datdrop_headers():
    return {'User-Agent' : 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36', 'X-Client-Version':'10'}

class DatBot(discord.Client):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.queue = asyncio.Queue()
        self.price_threshold = 100000

    async def on_ready(self):
        self.session = aiohttp.ClientSession()
        sio = socketio.AsyncClient()
        channel = self.get_channel(int(os.getenv('DISCORD_CHANNEL')))

        await channel.send(f'Starting DatBot with default threshold of **${self.price_threshold / 100:.2f}**')

        async def on_connect():
            print('connected to datdrop')
            await sio.emit('subscribe', {'channel' : 'group.games.lobby', 'auth' : {'headers' : {'X-Client-Id' : '1'}}})
            await sio.emit('ready')
        async def on_disconnect():
            print('disconnected from datdrop')
        async def on_message(event, namespace, data):
            if event == "group.games.opened":
                for game in data['games']:
                    price = sum([game['drop_cases'][str(round['drop_case_id'])]['price'] for round in game['game_rounds']])
                    await self.queue.put({'event' : 'game_start', 'uid' : game['uid'], 'creator' : game['creator']['nickname'], 'price' : price})
            elif event == "group.games.finished":
                for game in data['games']:
                    await self.queue.put({'event' : 'game_end', 'uid' : game['uid']})

        sio.on('connect', on_connect)
        sio.on('disconnect', on_disconnect)
        sio.on('*', on_message)
        await sio.connect(datdrop_url(), headers=datdrop_headers(), transports='websocket')

        active_games = set()
        while True:
            event = await self.queue.get()
            if event['event'] == 'game_start':
                if event['uid'] in active_games:
                    continue
                active_games.add(event['uid'])

                if event['price'] < self.price_threshold:
                    continue

                await channel.send(f'**{event["creator"]}** started **${event["price"] / 100:.2f}** at https://datdrop.com/battle/{event["uid"]}')
            elif event['event'] == 'game_end':
                active_games.discard(event['uid'])

    async def on_message(self, message):
        if message.author == self.user:
            return
        if message.channel.id != int(os.getenv('DISCORD_CHANNEL')):
            return
        if message.content.startswith('!limit'):
            await self.handle_limit(message)
        elif message.content.startswith('!profit'):
            await self.handle_profit(message)

    async def handle_limit(self, message):
        try:
            self.price_threshold = int(message.content.removeprefix('!limit').strip()) * 100
            await message.channel.send(f'Price threshold set to **${self.price_threshold / 100:.2f}**.')
        except ValueError:
            await message.channel.send(f'Usage: !limit <$ dollar amount>')

    async def handle_profit(self, message):
        steam_id = message.content.removeprefix('!profit').strip()
        if not steam_id.isnumeric():
            await message.channel.send(f'Usage: !profit <steam id>')
            return

        async with self.session.get(datdrop_user_url(steam_id), headers=datdrop_headers()) as resp:
            if resp.status != 200:
                await message.channel.send(f'**ERROR HTTP {resp.status}** for URL: {datdrop_user_url(steam_id)}')
                return
            data = await resp.json()
            user = data['steam_user']['nickname']

        message = await message.channel.send(f'<a:loading:1195565579650744511> calculating {user} profit...')

        errors = 0
        profit, offset = 0, 1
        while True:
            if errors > 3:
                await message.edit(content=f'<:painsga:725401763565273149> error calculating {user} profit: rate limited')
                return
            async with self.session.get(datdrop_history_url(steam_id, offset)) as resp:
                if resp.status == 429:
                    errors += 1
                    await asyncio.sleep(60)
                    continue
                elif resp.status != 200:
                    await message.edit(content=f'<:painsga:725401763565273149> error calculating {user} profit: http {resp.status}')
                    return
                errors = 0

                data = await resp.json()
                if len(data['games']) == 0:
                    break

                profit += sum([x['profit'] for x in filter(lambda x: list(filter(lambda y: y['steamid'] == steam_id, x['steam_users']))[0]['is_winner'], data['games'])])
                profit += -1 * sum([x['cost'] for x in filter(lambda x: not list(filter(lambda y: y['steamid'] == steam_id, x['steam_users']))[0]['is_winner'], data['games'])])
                offset += len(data['games'])
            await asyncio.sleep(4)

        await message.edit(content=f'{"<:toobased:938583712445132922>" if profit > 0 else "<:dump:1182040315700465804>"} **{user}** has earned **${profit / 100:.2f}** over {offset} battles.')
     

def init():
    dotenv.load_dotenv()

def main():
    intents = discord.Intents.default()
    intents.message_content = True

    bot = DatBot(intents=intents)
    bot.run(os.getenv('DISCORD_TOKEN'))

if __name__ == '__main__':
    init()
    main()
